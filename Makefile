.DEFAULT_GOAL := help
SHELL = /bin/bash

.PHONY: default build build_all build_7 build_5 push push_all push_5 push_7
default: help

build_all: build build_7_3 build_7_2 ## Build all images

build: ## Build latest image
	docker build ./7.3/. -t yonzin/php:latest

build_7_2: ## Build 7.2 image
	docker build ./7.2/. -t registry.gitlab.com/yonzin/php:7.2-test

build_7_3: ## Build 7.2 image
	docker build ./7.3/. -t yonzin/php:7.3

build_5: ## Build php 7 image
	docker build ./5.6/. -t yonzin/php:5.6

push_all: push push_7_2 push_7_3 ## push all images

push: ## push php latest image
	docker push yonzin/php:latest

push_7_3: push ## push php 7.3 image
	docker push yonzin/php:7.3

push_7_2:  ## push php 7.2 image
	docker push registry.gitlab.com/yonzin/php:7.2-test

push_5: ## push php 5.6 image
	docker push yonzin/php:5.6

## MISC
help: ## This help dialog.
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
	printf "%-30s %s\n" "Target" "Help" ; \
	printf "%-30s %s\n" "------" "----" ; \
	for help_line in $${help_lines[@]}; do \
		IFS=$$':' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf '\033[36m'; \
		printf "%-30s %s" $$help_command ; \
		printf '\033[0m'; \
		printf "%s\n" $$help_info; \
	done
